#!/usr/bin/env python
# -*- coding: utf-8 -*-
#       frage_und_antwort.py
#       
#       Copyright 2008 Christian Vervoorts <christian.vervoorts@dime-net.com>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
import random

#Wichtige anmerkung:
#Die Datei sollte das Format haben.
#Frage;Richtige antwort;Falsche Antwort;Falsche Antwort;Falsche Antwort;
#Jeweils einen Pro zeile. Beliebig viele Falsche antworten erlaubt.

def main():
    print "Willkommen zum Frage und Antwort Spiel"
    print
    file = raw_input('Name der Fragen-datei(oder Standardfile fragen.dat mit Enter): ')
    if file == '':
        file = 'fragen.dat'
    points = 0
    numberquestions = 0
    File_o = open(file)
    content = File_o.readlines()
    File_o.close()
    choose = range(len(content))
    random.shuffle(choose)
    for c in choose:
        line = content[c]
        question = line.strip().split(';')
        print
        print question.pop(0)
        numbers = range(len(question))
        random.shuffle(numbers)
        for i in range(len(question)):
            print "\t %i. %s" % (i+1 , question[numbers[i]])
        print
        antwort = int(raw_input('Antwort: '))
        #if question[numbers[antwort-1]] == question[0]: #Aufwändige version
        if numbers[antwort-1] == 0: #Einfache version, wieso komme ich da erst jetzt drauf *do'h*
            points += 1
            print "Super, richtig! Ein Punkt, du hast jetzt %i Punkt(e)" % points
        else:
            print "Leider Falsch. Kein Punkt, du hast %i Punkt(e)" % points
        numberquestions += 1
    
    print
    print "Spiel zuende!"
    print "Du hast %i von %i Punkten erspielt" % (points, numberquestions)      
    return 0

if __name__ == '__main__': main()
